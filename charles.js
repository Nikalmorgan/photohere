var options = {
    enableHighAccuracy: true,
    maximumAge: 0
};
const fallbackLocation = { latitude: 39.9548, longitude: 82.8121 }
let newElement = document.createElement("img")
function constructImageURL(photoObj) {
    return "https://farm" + photoObj.photos.photo.farm +
        ".staticflickr.com/" + photoObj.photos.photo.server +
        "/" + photoObj.photos.photo.id + "_" + photoObj.photos.photo.secret + ".jpg";
}
function showPhotos(dataObj) {
    newElement.src = constructImageURL(photoObj)
    document.body.appendChild(newElement)
}
// document.getElementById("button").onclick = showPhotos
function assembleSearchURL (coords) {
    const proxy = "https://shrouded-mountain-15003.herokuapp.com/"  //Randy's instance of cors
    return proxy +
    `https://flicker.com/services/rest/?` +
    `api_key=147e38a07d43dd6d1e958c1ed84ae68f` +
    `format=json&` +
    `nojsoncallback=1&` +
    `method-flickr.photos.search&` +
    `safe_search=1&` +
    `per_page=5&` +
    `page=1&` +
    `lat=${coords.latitude}&` +
    `lon=${coords.longitude}&` +
    `text=cars` 
};
function retreivePictures(coords) {
    console.log("Lat: " + coords.latitude)
    console.log("Lon: " + coords.longitude)
    let url = assembleSearchURL(coords)
    console.log(url)
    fetch(url)
        .then(response=> {
            return response.json()
        })
        .then(photoObj => {
            console.log(photoObj)
        })
}
function useRealLocation(pos) {
    retreivePictures(pos.coords)
}
function useFallbackLocation() {
    retreivePictures(fallbackLocation)
}
navigator.geolocation.getCurrentPosition(useRealLocation, useFallbackLocation, options);