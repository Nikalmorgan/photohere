//Global variables
const proxy = 'https://shrouded-mountain-15003.herokuapp.com/';
let h3 = document.querySelector('h3');
let pictureNum = 0;
let photosURL = [];
let photosHREF = [];
let locationNames = ['Dallas, Texas', 'Mexico City, Mexico', 'Colombia, Cuba', 'Paris, France', 'Moscow, Russia', 'Amsterdam, Netherlands'];
const defaultLocations = {
    latitude: [32.77815, 19.4284, 20.98806, 48.85341, 55.75222, 52.37403],
    longitude: [-96.7954, -99.12766, 77.42972, 2.3488, 37.61556, 4.88969]
};
let defaultSearch = "buildings";
let options = {
    enableHighAccuracy: true,
    maximumAge: 0
};

//Success function after getting location
function success(pos) {
    let crd = pos.coords;
    defaultSearch = prompt("What type of picture would you like to see?");
    proxyURL(crd);
    h3.innerHTML = `Your current position is: Latitude: ${crd.latitude} Longitude: ${crd.longitude} More or less ${crd.accuracy} meters`;
}
//Creates proxy url for fecth
function proxyURL(pos) {
    const URL = `https://flickr.com/services/rest/?` +
        `api_key=940cb9a24f7db15771133dbc58fe1a35` +
        `&format=json` +
        `&nojsoncallback=1` +
        `&method=flickr.photos.search` +
        `&safe_search=1` +
        `&per_page=5` +
        `&page=1;` +
        `&lat=${pos.latitude}` +
        `&lon=${pos.longitude}` +
        `&text=${defaultSearch}`;
    let url = `${proxy}${URL}`
    getPhotoData(url);
}
//Gets Photo data and displays data
function getPhotoData(url) {
    fetch(url)
        .then(response => {
            let promiseResults = response.json();
            return promiseResults;
        })
        .then(results => {
            let photos = results.photos.photo;
            createURLSHREFS(photos);
        })
        .catch((err) => {
            console.log(`Error message: ${err}`);
        })
}
//creates array of URLs and calls in display function
function createURLSHREFS(photos) {
    for (let index = 0; index < photos.length; index++) {
        photosURL.push(`https://farm${photos[index].farm}.staticflickr.com/${photos[index].server}/${photos[index].id}_${photos[index].secret}.jpg`);
    }
    for (let index = 0; index < photos.length; index++) {
        photosHREF.push([` https://www.flickr.com/photos/${photos[index].owner}/${photos[index].id}`, `${photos[index].title}`]);
    }
    displayPhoto(photosURL, photosHREF);
}
//displays photo
function displayPhoto(photosURL, photosHREF) {
    console.log(photosHREF)
    console.log(photosURL)
    let img = document.querySelector('.pic');
    console.log(photosURL[pictureNum])
    img.src = photosURL[pictureNum];
    let a = document.querySelector('.title');
    a.href = photosHREF[pictureNum];
    a.target = '_blank';
    console.log(photosHREF[pictureNum][1])
    a.innerHTML = photosHREF[pictureNum][1];
    console.log(pictureNum);
    console.log(photosHREF[pictureNum])

}
// //display previous photo 
function prevDisplay(photosURL, photosHREF) {
    if (pictureNum <= 0) {
        pictureNum = 0;
        alert(`You are on the first photo.`)
    } else {
        pictureNum--;
        displayPhoto(photosURL, photosHREF);
    }
}
// //display next photo
function nextDisplay(photosURL, photosHREF) {
    len = photosURL.length
    console.log(len)
    if (pictureNum >= len - 1) {
        alert(`You are on the last photo.`)
    } else {
        pictureNum++;
        displayPhoto(photosURL, photosHREF);
    }
}

function error(err) {
    console.warn(`ERROR(${err.code}): ${err.message}.`);
    let random = Math.floor(Math.random() * 5);
    let defaultLocation = { latitude: defaultLocations.latitude[random], longitude: defaultLocations.longitude[random] }
    h3.innerHTML = `Your current position is: Latitude: ${defaultLocation.latitude} Longitude: ${defaultLocation.longitude} which is ${locationNames[random]}.`;
    defaultSearch = prompt("What type of picture would you like to see?");
    // if (defaultSearch === '') defaultSearch = "buildings"
    proxyURL(defaultLocation);
    // let defaultURl = proxy + "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=27c8dba4ba49b2a7554290ed7d113e73&lat=32.7767&lon=96.7970&format=json&nojsoncallback=1&text=food&auth_token=72157712775257338-11eb89c40e537df8&api_sig=f47bcd53a32004e760e6fb107e3ab857";
    // getPhotoData(defaultURl);
}
navigator.geolocation.getCurrentPosition(success, error, options);